﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Highscores_wegschrijven_ghost_riding
{
    public partial class HighscoreForm : Form
    {
        public HighscoreForm()
        {
            InitializeComponent();
        }

        int score = 69;

        private void button1_Click(object sender, EventArgs e)
        {
            string sourcepath = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string Highscores = Path.Combine(sourcepath, "C:/Users/" +
            Environment.UserName + "/Desktop/Website GhostRiding/Highscores.html");
            StreamReader inputStream = File.OpenText(Highscores);
            string line = inputStream.ReadLine();

            inputStream.Close();
            string destination = Environment.GetFolderPath(Environment.SpecialFolder.Desktop);
            string NieuweHighscores = Path.Combine(destination, "C:/Users/"  +
            Environment.UserName + "/Desktop/Website GhostRiding/Highscores.html");
            StreamWriter outputStream = File.CreateText(NieuweHighscores);
            outputStream.WriteLine(line + "<br /><p style='text-align: left; display: inline; padding-left: 10px'>{0} uit {1}</p><p style='text-align: center; display: inline; padding-left: 55%;'>{2}</p>", 
                textBox1.Text, textBox2.Text, score);
            outputStream.Close();
            this.Close();
        }
    }
}
